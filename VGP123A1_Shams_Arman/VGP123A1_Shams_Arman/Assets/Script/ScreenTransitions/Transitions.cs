﻿using UnityEngine;
using System.Collections;

public class Transitions : MonoBehaviour {

	public int transitionNo;	// 0 = screen 1 to screen 2, 1 = screen 3 to screen 4 transition.

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Player" && transitionNo == 0)
		{
			coll.GetComponent<Transform>().position = new Vector2(-47.95f, -2.86f);
			coll.GetComponent<Character_Controller>().canScroll = false;
			Camera.main.transform.position = new Vector3(-40.79f, -0.77f, Camera.main.transform.position.z);
		}

		if (coll.gameObject.tag == "Player" && transitionNo == 1)
		{
			if (coll.gameObject.transform.position.y < transform.position.y)
			{
				Debug.Log("Player should have poofed up.");
				coll.GetComponent<Transform>().position = new Vector2( (coll.GetComponent<Transform>().position.x + 0.5f) , -2.86f);
				Camera.main.transform.position = new Vector3(coll.GetComponent<Transform>().position.x, -0.77f, Camera.main.transform.position.z);
				
			}
			else if (coll.gameObject.transform.position.y > transform.position.y)
			{
				Debug.Log("Player should have poofed down.");
				coll.GetComponent<Transform>().position = new Vector2(coll.GetComponent<Transform>().position.x, -6.27f);
				Camera.main.transform.position = new Vector3(20.9f, -8.91f, Camera.main.transform.position.z);
			}

		}

	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
