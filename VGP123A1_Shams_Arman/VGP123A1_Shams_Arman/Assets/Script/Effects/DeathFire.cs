﻿using UnityEngine;
using System.Collections;

public class DeathFire : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Animator>().Play("DeathFire");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// animDestroy is called once the fire's animation has finished playing.
	void animDestroy()
	{
		Destroy(gameObject);
	}
}
