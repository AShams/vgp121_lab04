﻿using UnityEngine;
using System.Collections;

public class SpecialWeapon: MonoBehaviour {

	// This script is used by all Special Weapon prefabs. Values are all modified on individual prefabs in the inspector.
	// To be used on an object the object requires a boxCollider2D, Sprite renderer, rigidbody2D and an animator if the item requires animation.

	public float lifeTime;				// How long a projectile will exist
	public float xSpeed;				// Speed on the X axis
	public float ySpeed;				// Speed on the Y axis
	public bool isBoomerang = false;	// Set to true if the object has the boomerangs properties (flying backward toward the player)
	public bool isHolyWater = false;	// Set to true if the object has the holy water's properties (exploding on impact with a collider)
	public float direction;             // Direction assigns the direction of the projectile to be the same as the player character.
	GameObject player;                  // Used to access and modify the player's maxThrownWeapons
	bool boomerangCanRetun;				// Prevents boomerang from infinitely flying back and forth. Is set to true if the attached object isBoomerang.


	// On an object that is holy water's box collider should not be marked as a trigger.
	void OnCollisionEnter2D (Collision2D coll)
	{
		// If the player has holy water equipped the holy water will combust and the flames will despawn after a period of time.
		if (isHolyWater && (coll.gameObject.tag == "Ground"))
		{
			Debug.Log("Holy water hit a wall, should play it's animation for burning, then despawn.");
			GetComponent<Animator>().Play("HolyWaterFire");
		}

	}

	// Weapons in castlevania, aside from holy water, ignore collision with terrain.
	void OnTriggerEnter2D(Collider2D coll)
	{

		// If the player has a boomerang equipped it will be picked up by the player on collision with an object tagged "Player"
		if (isBoomerang && (coll.gameObject.tag == "Player"))
			Destroy(gameObject);
	}

	// When the special weapon is destroyed, currentThrownWeapons is positively incremented.
	void OnDestroy()
	{
		player.GetComponent<Character_Controller>().currentThrownWeapons += 1;
	}

	// Use this for initialization
	void Start()
	{

		player = GameObject.FindGameObjectWithTag("Player");

		// Animator's default animation is ThrowingAxe
		// Sets the animation to "HolyWater" if the object is holy water.
		if (isHolyWater)
			GetComponent<Animator>().Play("HolyWaterFire");

		// Sets animation to Boomerang if the thrown weapon is a boomerang.
		if (isBoomerang)
		{
			GetComponent<Animator>().Play("Boomerang");
			boomerangCanRetun = true;
		}
			
		// Check that lifeTime isn't negative or 0 & sets a default value if one does not exist.
		if (lifeTime <= 0)
			lifeTime = 1.0f;

		// Set the objects direction
		transform.localScale = new Vector3(direction, 1.0f, 1.0f);

		// Make projectile move when instantiated.
		GetComponent<Rigidbody2D>().velocity = new Vector2((direction * xSpeed), ySpeed);

		// Negatively increment maxThrownWeapons.
		player.GetComponent<Character_Controller>().currentThrownWeapons -= 1;


		// Destroy projectile GameObject if it doesn't collide with anything.
		Destroy(gameObject, lifeTime);

		Debug.Log(direction);

	}

	// Update is called once per frame
	void Update () {

		// Boomerang flies back toward the player after depending on direction.
		if (transform.localPosition.x < (player.GetComponent<Transform>().localPosition.x - 2.5f) && isBoomerang && boomerangCanRetun)
		{
			GetComponent<Rigidbody2D>().velocity = new Vector2(((-direction) * xSpeed), ySpeed);
			boomerangCanRetun = false;
		}

		else if (transform.position.x > (player.GetComponent<Transform>().localPosition.x + 2.5f) && isBoomerang && boomerangCanRetun)
		{
			GetComponent<Rigidbody2D>().velocity = new Vector2(((-direction) * xSpeed), ySpeed);
			boomerangCanRetun = false;
		}

	}
}
