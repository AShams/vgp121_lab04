﻿using UnityEngine;
using System.Collections;

public class Character_Controller : MonoBehaviour {


	Rigidbody2D rb;							// Player Character's rigidbody2D.
	Animator anim;							// Player Character's animator component.
	BoxCollider2D hitbox;                   // Manipulates player's hitbox.
	Transform weaponSpawnPoint;             // Location where the throwing weapons are spawned.
	Transform spritePosition;               // Gets the player's sprite's transform component to reposition the player's sprite for consistent animation.
	Transform groundCheck;                  // Used to access the size and position of the object checking the ground.
	public GameObject whipHitBox;			// GameObject toggles when the whipHitBox is active, and set it's size.
	public LayerMask isOnGround;            // LayerMask detects whether or not it is over an object on layer 9(ground) to set whether isGrounded is true/false
	bool isFacingLeft;                      // Tracks player facing. Starts at false, the player faces right at spawn.
	public bool isGrounded;                 // Tracks if the player is standing on something on the ground layer.
	public bool canScroll;					// True = camera can scroll left and right based on player's position, false = camera cannot scroll.
	public float speed;                     // Handle movement speed of character. Can be adjusted through inspector.
	public float jumpHeight;                // Stores a float that affects the height of the player's jump. Can be adjusted through inspector.
	public int whipLevel;                   // Stores the current whip the player has. 0 = whip, 1 = chain whip, 2 = long whip
	public int hearts;                      // Stores the players current hearts (ammo).
	public int points;                      // Integer tracks the players current score.
	public int health;                      // Player's health, player dies when it reaches 0.
	public int maxThrownWeapons;            // Integer tracks the maximum number of throwing weapons the player can have in existance at a given time.
	public int currentThrownWeapons;		// Integer tracks the current number of weapons on screen.
	public bool canClimbStairs;				// Tracks whether or not the player can climb stairs.
	public bool onStairs = false;           // Tracks whether or not the player is on stairs.
	public LayerMask isOnStairs;            // LayerMask determines if the player is on an object tagged as "Stairs"
	

	// Stores an integer based on the current throwing weapon the player has in their inventory.
	// 0 = no weapon, 1 = throwing knife, 2 = throwing axe, 3 = boomerang, 4 = holy water, 5 = stopwatch.
	public int equippedSpecialWeapon;
	

	void OnTriggerEnter2D(Collider2D coll)
	{	
		// Determines whether the player is in an area where the camera should not scroll.
		if (coll.gameObject.tag == "noCameraScroll")
			canScroll = false;
	}
	void OnTriggerStay2D(Collider2D coll)
	{
		// Determines whether the player is in an area where the camera should not scroll.
		if (coll.gameObject.tag == "noCameraScroll")
			canScroll = false;
	}
	void OnTriggerExit2D(Collider2D coll)
	{
		// Allows the camera to scroll when the player exits a 'no scroll zone'
		if (coll.gameObject.tag == "noCameraScroll")
			canScroll = true;
	}

	// Use this for initialization
	void Start () {

		// Components are referenced and initialized.
		rb = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		hitbox = GetComponent<BoxCollider2D>();
		groundCheck = this.gameObject.transform.GetChild(2);
		spritePosition = this.gameObject.transform.GetChild(1);
		weaponSpawnPoint = this.gameObject.transform.GetChild(3);

		// isFacingLeft is initialized to false (The character begins feacing right.)
		isFacingLeft = false;

		canScroll = true;

		// maxThrownWeapons and currentThrownWeapons are initialized to 1. While a throwing weapon exists on screen currentThrownWeapons will be negatively incremented.
		maxThrownWeapons = 1;
		currentThrownWeapons = 1;

		// Checks if Rigidbody2D exists. 
		if (!rb)
			Debug.LogWarning("No Rigidbody2D found on Character.");

		// Checks if an Animator exists.
		if (!anim)
			Debug.Log("Missing animator component.");

		// Failsafe checks if speed is set and sets speed to a default value (5.0f)
		if (speed == 0) 
		{
			Debug.LogWarning("Speed not set in inspector, defaulting to 5.");
			speed = 3.0f;
		}
		// Failsafe checks if jumpHeight is set and sets it to a default value (10.0f)
		if (jumpHeight == 0)
		{
			Debug.LogWarning("jumpHeight not set, defaulting to 10");
			jumpHeight = 3.5f;
		}
		if (health == 0)
			health = 10;

		hearts = 10;
	}
	
	// Update is called once per frame
	void Update () {

		float moveValue = Input.GetAxisRaw("Horizontal");

		//Debug.Log(moveValue);
		// Main camera's position is set to the same as the player's on the X axis, Y and Z axis are left the same. The Y axis is altered by the transition objects.
		if (canScroll)
			Camera.main.transform.position = new Vector3(GetComponent<Transform>().position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);

		// Anim Speed is determined.
		anim.SetFloat("Speed", Mathf.Abs(moveValue));

		// isGrounded is initialized.
		isGrounded = Physics2D.OverlapArea((groundCheck.position + new Vector3(0.2f, 0.2f, 0.0f)), (groundCheck.position - new Vector3 (0.2f, 0.2f, 0.0f)), isOnGround);

		// Flips the players sprite based on moveValue, only if the player is grounded and isn't attacking.
		if ((moveValue > 0 && isFacingLeft && isGrounded && !anim.GetBool("Attack")) || (moveValue < 0 && !isFacingLeft && isGrounded && !anim.GetBool("Attack")))
			invertX();

		// canClimbStairs is initialized
		canClimbStairs = Physics2D.OverlapArea((groundCheck.position + new Vector3(0.05f, 0.05f, 0.0f)), (groundCheck.position - new Vector3(0.05f, 0.05f, 0.0f)), isOnStairs);

		// Character moves based on moveValue
		if (isGrounded && !anim.GetBool("Attack") && !anim.GetBool("Crouch"))
			rb.velocity = new Vector2(moveValue * speed, rb.velocity.y);

		if (Input.GetButtonDown("Jump") && isGrounded && !anim.GetBool("Jump"))
		{
				anim.SetBool("Jump", true);
				rb.AddForce(Vector2.up * jumpHeight, ForceMode2D.Impulse);
				Debug.Log("Jump");
		}
		else if(isGrounded && anim.GetBool("Jump"))
			anim.SetBool("Jump", false);

		// If crouch is pressed and the player is on solid ground the player's hitbox is slightly offset,
		// the hitbox is resized, player's sprite is moved in a co-responding fashion, 
		// the player's sprite position is adjusted, the whip's hitbox, and weaponSpawnPoint are moved.
		// rb's velocity is set to 0.0f on the x axis and stops the player's movement.
		if (Input.GetButton("Crouch") && isGrounded && !anim.GetBool("Attack"))
		{
			hitbox.size = new Vector2(hitbox.size.x, 0.88f);
			hitbox.offset = new Vector2(0.0f, -0.14f);
			spritePosition.localPosition = new Vector2(0.0f, -0.14f);
			rb.velocity = new Vector2(0.0f, rb.velocity.y);
			weaponSpawnPoint.localPosition = new Vector2(-0.5f, -0.15f);
			whipHitBox.GetComponent<Transform>().localPosition = new Vector2(-0.5f, -0.128f);
			anim.SetFloat("Speed", 0.0f);
			anim.SetBool("Crouch", true);
		}
		else if (!anim.GetBool("Attack"))
		{ 
			hitbox.size = new Vector2(hitbox.size.x, 1.17f);
			hitbox.offset = new Vector2(0.0f, 0.0f);
			spritePosition.localPosition = new Vector2(0.0f, 0.0f);
			weaponSpawnPoint.localPosition = new Vector2(-0.5f, 0.15f);
			whipHitBox.GetComponent<Transform>().localPosition = new Vector2(-0.5f, 0.128f);
			anim.SetBool("Crouch", false);
		}

 
		if (onStairs)
			rb.isKinematic = true;
		// Several attempts at making stairs were fruitless and buggier than the current implementation. 
		// One instance of the attempt at stairs was left in the project.
		if (canClimbStairs)
		{
			if (Input.GetButton("Up"))
			{
				onStairs = true;
				transform.Translate(new Vector3(2.5f, 2.5f, 0.0f) * Time.deltaTime);
			}

			if (Input.GetButton("Crouch"))
			{
				onStairs = true;
				transform.Translate(new Vector3(-2.5f, -2.5f, 0.0f) * Time.deltaTime);

			}
		}
		if (onStairs && isGrounded)
		{
			onStairs = false;
			rb.isKinematic = false;
		}
			

		// If the player isn't attacking and presses "Fire1", plays co-responding attack animation based on current Whip.
		if (Input.GetButtonDown("Fire1") && !anim.GetBool("Attack"))
		{
			// Stops player's movement if they're on the ground.
			if (isGrounded)
				rb.velocity = new Vector2(0.0f, rb.velocity.y);

			// Checks to see if the player is holding up at the time of input and calls useSpecialWeapon
			if (Input.GetButton("Up") && hearts > 0 && equippedSpecialWeapon != 0 && currentThrownWeapons <= maxThrownWeapons && currentThrownWeapons > 0)
			{
				if (anim.GetBool("Crouch"))
					anim.Play("PlayerCharacter_Crouch_ThrowWeapon");
				else
					anim.Play("PlayerCharacter_Attack_ThrowWeapon");
			}
			else
				attack();
		}
	}

	// reads the players current transform.localscale and replaces the X value with -X
	void invertX()
	{
		// Toggle isFacingLeft
		isFacingLeft = !isFacingLeft;

		// A new vector 3 is needed when accessing transform.localScale directly.
		// Vector 3 invert stores the current value of localScale, then replaces the current x value with -x
		Vector3 invert = transform.localScale;
		invert.x *= -1;
		transform.localScale = invert;
	}

	// useSpecialWeapon() determines which special weapon needs to be instantiated and spawns it when it's called on in the animator.
	void useSpecialWeapon()
	{
		// A GameObject called instance is created
		GameObject instance;

		// Based on the special weapon equipped the direction, Speed at which it moves on the x and y axis. 
		// The correct Prefab will be instantiated based on the special weapon equipped.
		// All throwing weapon prefabs are stored in Resoures/Prefabs/SpecialWeapons
		// -(GetComponent<Transform>().localScale.x); is used because the characters default x is -1
		// Lifetime, xSpeed and ySpeed are all set to their appropriate values based on the prefab spawned.
		// Booleans that initialize unique behaviour for holy water and boomerangs are toggled if the player's
		// current special weapon is the holy water or boomerang respectively.
		switch (equippedSpecialWeapon)
		{
			
		
			// Instantiates a ThrowingKnife and subtracts hearts accordingly.
			case 1:
				instance = Instantiate(((GameObject)Resources.Load("Prefabs/SpecialWeapons/ThrowingKnife")), weaponSpawnPoint.position, weaponSpawnPoint.rotation) as GameObject;
				instance.GetComponent<SpecialWeapon>().direction = -(GetComponent<Transform>().localScale.x);
				instance.GetComponent<SpecialWeapon>().lifeTime = 5.0f;
				instance.GetComponent<SpecialWeapon>().xSpeed = 5.0f;
				instance.GetComponent<SpecialWeapon>().ySpeed = 0.0f;
				Physics2D.IgnoreCollision(GetComponent<Collider2D>(), instance.GetComponent<Collider2D>());
				hearts -= 1;
				break;

			// Instantiates a ThrowingAxe and subtracts hearts accordingly.
			case 2:
				instance = Instantiate(((GameObject)Resources.Load("Prefabs/SpecialWeapons/ThrowingAxe")), weaponSpawnPoint.position, weaponSpawnPoint.rotation) as GameObject;
				instance.GetComponent<SpecialWeapon>().direction = -(GetComponent<Transform>().localScale.x);
				instance.GetComponent<SpecialWeapon>().lifeTime = 5.0f;
				instance.GetComponent<SpecialWeapon>().xSpeed = 2.0f;
				instance.GetComponent<SpecialWeapon>().ySpeed = 8.0f;
				Physics2D.IgnoreCollision(GetComponent<Collider2D>(), instance.GetComponent<Collider2D>());
				hearts -= 1;
				break;

			// Instantiates a Boomerang and subtracts hearts accordingly. 
			case 3:
				instance = Instantiate(((GameObject)Resources.Load("Prefabs/SpecialWeapons/ThrowingBoomerang")), weaponSpawnPoint.position, weaponSpawnPoint.rotation) as GameObject;
				instance.GetComponent<SpecialWeapon>().direction = -(GetComponent<Transform>().localScale.x);
				instance.GetComponent<SpecialWeapon>().lifeTime = 5.0f;
				instance.GetComponent<SpecialWeapon>().xSpeed = 6.0f;
				instance.GetComponent<SpecialWeapon>().ySpeed = 0.0f;
				hearts -= 2;
				break;

			// Instantiates HolyWater and subtracts hearts accordingly.
			case 4: 
				instance = Instantiate(((GameObject)Resources.Load("Prefabs/SpecialWeapons/ThrowingHolyWater")), weaponSpawnPoint.position, weaponSpawnPoint.rotation) as GameObject;
				instance.GetComponent<SpecialWeapon>().direction = -(GetComponent<Transform>().localScale.x);
				instance.GetComponent<SpecialWeapon>().lifeTime = 5.0f;
				instance.GetComponent<SpecialWeapon>().xSpeed = 2.0f;
				instance.GetComponent<SpecialWeapon>().ySpeed = 0.0f;
				Physics2D.IgnoreCollision(GetComponent<Collider2D>(), instance.GetComponent<Collider2D>());
				hearts -= 3;
				break;

			// Will use Stopwatch when enemies are implemented.
			case 5:
				// The stopwatch freezes enemies in place. Once enemies are implemented the stopwatch can be completed.
				Debug.Log("Player used stopwatch.");
				break;


			default:
				Debug.LogError("useSpecialWeapon is being called when the player has no special weapon");
				break;


		}
	}

	// attack() determines whether or not the player is crouching then plays co responding animation based on whipLevel
	void attack()
	{
		// switch statement checks which whip the player currently has, then checks whether or not they are crouching or on stairs. 
		switch (whipLevel)
		{
			case 0:
				if (anim.GetBool("Crouch"))
					anim.Play("PlayerCharacter_Crouch_Attack_Whip");
				else
					anim.Play("PlayerCharacter_Attack_Whip");
				break;

			case 1:
				if (anim.GetBool("Crouch"))
					anim.Play("PlayerCharacter_Crouch_Attack_ChainWhip");
				else
					anim.Play("PlayerCharacter_Attack_ChainWhip");
				break;

			case 2:
				if (anim.GetBool("Crouch"))
					anim.Play("PlayerCharacter_Crouch_Attack_LongWhip");
				else
					anim.Play("PlayerCharacter_Attack_LongWhip");
				break;
		}		
	}
	// whipHitCheck() sizes and enables a trigger collider that checks if the player has hit an object that can be damaged or destroyed.
	// used in the animator to enable the hitbox on the frame in which the whip is fully extended.
	void whipHitCheck()
	{
		// The whips hitbox's size and offset are adjusted according to whip level.
		switch (whipLevel)
		{
			case 0:
				whipHitBox.GetComponent<BoxCollider2D>().offset = new Vector2(-0.3f, 0.0f);
				whipHitBox.GetComponent<BoxCollider2D>().size = new Vector2(1.4f, 0.3f);
				break;

			case 1:
				whipHitBox.GetComponent<BoxCollider2D>().offset = new Vector2(-0.3f, 0.0f);
				whipHitBox.GetComponent<BoxCollider2D>().size = new Vector2(1.4f, 0.3f);
				break;

			case 2:
				whipHitBox.GetComponent<BoxCollider2D>().offset = new Vector2(-0.57f, 0.0f);
				whipHitBox.GetComponent<BoxCollider2D>().size = new Vector2(2.0f, 0.3f);
				break;
		}
		// whipHitBox.enabled is toggled twice during each attack animation.
		whipHitBox.GetComponent<BoxCollider2D>().enabled = !whipHitBox.GetComponent<BoxCollider2D>().enabled;
	}

	// Called when the player collides with a cross. Finds and destroys all objects with tag "Enemy"
	public void destroyAllEnemiesOnScreen()
	{
		// An array of GameObject with the Tag "Enemy" is created and each object within is destroyed.
		GameObject[] enemies;
		enemies = GameObject.FindGameObjectsWithTag("Enemy");

		for (var i = 0; i < enemies.Length; i++)
		{
			Destroy(enemies[i]);
		}
	}

	// animAttackToggle is defined as a function so it can be called on in an animator event.
	// Toggles animator boolean "Attack" at the start and end of the attack animation.
	void animAttackToggle()
	{
		anim.SetBool("Attack", !anim.GetBool("Attack"));
	}

}

