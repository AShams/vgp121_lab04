﻿using UnityEngine;
using System.Collections;

public class CollectibleItems : MonoBehaviour {

	// This script is used by all collectible item prefabs. Values are all modified on individual prefabs in the inspector.
	// To be used on an object the object requires a boxCollider2D, Sprite renderer, rigidbody2D and an animator if the item requires animation.

	public int heartValue;						// Amount of hearts the player will recieve on collision.                                                                                                                                            
	public int pointValue;						// Amount of points the player will recieve on collision.   
	public bool isWhipUpgrade;					// Set to true if the collectible object is a whip upgrade.
	public bool isDoubleShot;					// Set to true if the collectible object is a doubleshot.
	public bool isTripleShot;					// Set to true if the collectible object is a tripleshot.
	static Animation anim;						// Stops animations (hearts moving in a wave pattern) when the item lands on the ground.
	public float lifeTime;						// How long an item will last on the ground before despawning.
	public int healthValue;						// How much health the collectible object should restore.
	public bool isCross;						// Whether or not the object is a cross
	public bool isLevelEnd = false;				// true = collecting object ends level, false = collecting object does not end level.
	public bool isInvincibilityPotion = false;	// true = collecting object makes player invulnerable to damage for a period of time.

	// Stores an integer based on which weapon should be placed in the character's inventory on collision.
	// 0 = this object is not a weapon, 1 = throwing knife, 2 = throwing axe, 3 = boomerang, 4 = holy water, 5 = stopwatch.
	public int weaponType;

	void OnCollisionEnter2D (Collision2D coll)
	{
		// If the object has an animation component and an animation that needs to stop on collision with the ground the default animation will stop playing.
		if (anim && !isLevelEnd)
			anim.Stop();

		// plays the appropriate animation if the object is a level ending orb (dropped by bosses)
		if (isLevelEnd)	
			// Currently there is no credit screen, when a credit screen is implemented colliding with a level ending object will switch scenes accordingly.
			GetComponent<Animator>().Play("LevelEndOrb");
			

		if (coll.gameObject.tag == "Player")
		{
			// If the collectible object is a weapon when the player collides with this object the current equipped weapon
			// is set according to the weapon object.
			if (weaponType != 0)
				coll.gameObject.GetComponent<Character_Controller>().equippedSpecialWeapon = weaponType;

			// If the collectible object is a whip upgrade whipLevel is positive incremented.
			if (isWhipUpgrade && coll.gameObject.GetComponent<Character_Controller>().whipLevel < 2)
				coll.gameObject.GetComponent<Character_Controller>().whipLevel++;

			// If the collectible object is a doubleshot or triple shot maxThrown weapons is increased.
			// Torches and destructible objects can only spawn a doubleshot or triple shot if the player does not already have one.
			if (isDoubleShot)
				coll.gameObject.GetComponent<Character_Controller>().maxThrownWeapons = 2;

			if (isTripleShot)
				coll.gameObject.GetComponent<Character_Controller>().maxThrownWeapons = 3;

			// If the object is a cross it will destroy all game objects tagged as "Enemy"
			if (isCross)
				coll.gameObject.GetComponent<Character_Controller>().destroyAllEnemiesOnScreen();

			// Makes player invulnerable when collided with.
			if (isInvincibilityPotion)
				// To make the invincibility potion function, enemies need to be completed. 
				Debug.Log("Player collected invincibility potion.");


			// The heart and point values are added to the player's current hearts and points.
			// A heart prefab should have a pointValue of 0, a point bag should have a heartValue of 0.
			// Health pickups should only have a healthValue.
			coll.gameObject.GetComponent<Character_Controller>().points += pointValue;
			coll.gameObject.GetComponent<Character_Controller>().hearts += heartValue;
			coll.gameObject.GetComponent<Character_Controller>().health += healthValue;

			Destroy(gameObject);
		}
	}


	// Use this for initialization
	void Start() {
		anim = GetComponent<Animation>();
		// Check that lifeTime isn't negative or 0 & sets a default value if one does not exist.
		if (lifeTime <= 0)
			lifeTime = 5.0f;

		// Destroy the object if it's been left on the ground for too long and isn't a level ending collectible.
		if (!isLevelEnd)
			Destroy(gameObject, lifeTime);
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	
}
