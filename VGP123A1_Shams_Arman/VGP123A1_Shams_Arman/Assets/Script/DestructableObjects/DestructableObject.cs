﻿using UnityEngine;
using System.Collections;

public class DestructableObject: MonoBehaviour {


	public GameObject player;			// Used to reference the player's whipLevel.
	public bool torchType;				// true = tall torch, false = wall torch.
	public bool isDesctructibleBlock;	// true = object is a destrucible block, false = object is a torch.
	Animator anim;						// Used to switch between sprites and animations for wall torches and tall torches.


	// Stores what item the destructible object stores.
	// 0 = whip upgrade, 1 = small heart, 2 = large heart, 3 = throwing knife, 4 = throwing axe
	// 5 = boomerang, 6 = holy water, 7 = stopwatch, 8 = cross, 9 = invincibility potion
	// 10 = hundred point bag, 11 = four hundered point bag, 12 = seven hundred point bag
	// 13 = thousand point bag, 14 = doubleshot 15 = triple shot, 16 = health pickup
	public int containedItem;

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "PlayerWeapon")
		{
			Debug.Log("The object has been hit by the player's weapon.");
			// A GameObject is created, based on the contained item the appropriate prefab will be instantiated and the torch object destroyed.
			GameObject instance;
			switch (containedItem)
			{
				// Instantiate a Whip Upgrade.
				case 0:
					// In castlevania whip upgrades can only spawn if the player doesn't already have a fully upgraded whip. In the event their whip is maxed a 100 point bag spawns.
					if (player.GetComponent<Character_Controller>().whipLevel < 2)
						instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_WhipUpgrade")), transform.position, transform.rotation) as GameObject;
					else
						instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_HundredPointBag")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a Small Heart, whip upgrade(if whip isn't fully upgraded), or a hundred point bag.
				case 1:
					// In castlevania all torches containing a small heart have a chance to contain a whip upgrade(if the whip isn't fully upgraded) or a hundred point bag.
					// randTorch generates a random number between 1-3
					int randTorch = Mathf.CeilToInt((Random.value) * 3);
					Debug.Log(randTorch);

					switch (randTorch)
					{
						// Cases 0 and 1 instantiate a small heart to make the chances of getting a small heart greater than all other options.
						case 0:
							instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_SmallHeart")), transform.position, transform.rotation) as GameObject;
							break;

						case 1:
							instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_SmallHeart")), transform.position, transform.rotation) as GameObject;
							break;

						// If a whip upgrade can be instantiated, it will be. Otherwise a small heart is instantiated.
						case 2:
							if (player.GetComponent<Character_Controller>().whipLevel < 2)
								instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_WhipUpgrade")), transform.position, transform.rotation) as GameObject;

							else
								instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_SmallHeart")), transform.position, transform.rotation) as GameObject;
							break;

						// Instantiates a hundred point bag.
						case 3:
							instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_HundredPointBag")), transform.position, transform.rotation) as GameObject;
							break;
					}
					break;

				// Instantiate a Large Heart.
				case 2:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_LargeHeart")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate the collectible knife.
				case 3:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_Knife")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate the collectible axe.
				case 4:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_Axe")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a collectible boomerang.
				case 5:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_Boomerang")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a collectible holy water.
				case 6:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_HolyWater")), transform.position, transform.rotation) as GameObject;
					break;
	
				// Instantiate a collectible Stopwatch
				case 7:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_StopWatch")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a collectible Cross (Clears all enemies on screen)
				case 8:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_Cross")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a collectible Invincibility potion
				case 9:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_InvincibilityPotion")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a hundred point bag.
				case 10:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_HundredPointBag")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a four hundred point bag.
				case 11:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_FourHundredPointBag")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a seven hundred point bag.
				case 12:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_SevenHundredPointBag")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a seven hundred point bag.
				case 13:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_ThousandPointBag")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a doubleshot.
				case 14:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_Doubleshot")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a tipleshot
				case 15:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_TripleShot")), transform.position, transform.rotation) as GameObject;
					break;

				// Instantiate a healthpickup
				case 16:
					instance = Instantiate(((GameObject)Resources.Load("Prefabs/Collectibles/Collectible_Health")), transform.position, transform.rotation) as GameObject;
					break;

				// Use this if the object contains nothing.
				default:
					break;

			}

			if (!isDesctructibleBlock)
				instance = Instantiate(((GameObject)Resources.Load("Prefabs/WorldObjects/DeathFire")), transform.position, transform.rotation) as GameObject;
			Destroy(gameObject);

		}

	}


	// Use this for initialization
	void Start () {
		// Player and animator are initialized.
		player = GameObject.FindGameObjectWithTag("Player");
		anim = GetComponent<Animator>();

		if (!torchType && !isDesctructibleBlock)
			anim.Play("WallTorch");

		if (torchType && !isDesctructibleBlock)
			anim.Play("Torch");


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
